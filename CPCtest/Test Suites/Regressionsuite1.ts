<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regressionsuite1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d10ccd1c-5ca9-4317-ab68-ce7843f9ef64</testSuiteGuid>
   <testCaseLink>
      <guid>e9cca592-f18a-412e-b4a5-6fe52e509a2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Track/Track_003</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f016a472-7f53-4f31-811b-3d591624fee1</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-1</value>
         </iterationEntity>
         <testDataId>Data Files/Track/ProdPINs</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f016a472-7f53-4f31-811b-3d591624fee1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TrackingPRODNum</value>
         <variableId>69edaaf4-ffc2-4694-99b2-cfe2f1b64865</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
