package cpc.mobile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class startapp {



	@Keyword
	def startApptrue(){

		Mobile.startApplication('//Users/matthews/Library/Developer/Xcode/DerivedData/CPCTools-dtpxfviqodylejanqwbyilwpxzsj/Build/Products/Debug-iphonesimulator/CPCTools.app',
				true)
		if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL) ==
		true) {
			Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 3)

			Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'), 30)
		}

		if (Mobile.verifyElementExist(findTestObject('Track/txtBtn-Cancel'), 20, FailureHandling.OPTIONAL) ==
		true){

			Mobile.tap(findTestObject('Track/txtBtn-Cancel'), 30)
		}
	}

	@Keyword
	def verifyDisplayElemment(String expected, String actual){

		println("Expected:" +expected)
		println("Actual:" +actual)

		String boole = Mobile.verifyEqual(expected, actual)

		if(boole==true){

			KeywordUtil.markPassed("Expected   " + expected + "and Actual  " + actual + "are Equal")
		}

		else{
			KeywordUtil.markFailed("Expected  " + expected + "and Actual   " + actual + "are NOT Equal")
		}
		//Mobile.verifyEqual(expected, actual)

	}
	/**
	 * Check if element present in timeout
	 * @param to Katalon test object
	 * @param timeout time to wait for element to show up
	 * @return true if element present, otherwise false
	 */
	@Keyword
	def isElementPresent_Mobile(TestObject to, int timeout){
		try {
			KeywordUtil.logInfo("Finding element with id:" + to.getObjectId())

			WebElement element = MobileElementCommonHelper.findElement(to, timeout)
			if (element != null) {
				KeywordUtil.markPassed("Object " + to.getObjectId() + " is present")
			}
			return true
		} catch (Exception e) {
			KeywordUtil.markFailed("Object " + to.getObjectId() + " is not present")
		}
		return false;
	}




	/**
	 * Get mobile driver for current session
	 * @return mobile driver for current session
	 */
	@Keyword
	def WebDriver getCurrentSessionMobileDriver() {
		return MobileDriverFactory.getDriver();
	}
}