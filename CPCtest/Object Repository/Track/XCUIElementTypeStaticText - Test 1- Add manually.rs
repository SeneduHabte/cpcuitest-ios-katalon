<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>XCUIElementTypeStaticText - Test 1- Add manually</name>
   <tag></tag>
   <elementGuidId>bea7e3a7-b826-4e8b-87e6-ab5a829a6f5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'XCUIElementTypeStaticText' and @label = 'Test 1- Add manually' and @name = 'Test 1- Add manually']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@type = 'XCUIElementTypeStaticText' and @label = 'Test 1- Add manually' and @name = 'Test 1- Add manually']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>label</name>
      <type>Main</type>
      <value>Test 1- Add manually</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Test 1- Add manually</value>
   </webElementProperties>
</WebElementEntity>
