<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imgBtn-TrackItem</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//XCUIElementTypeButton[@name=&quot;Track an Item&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//XCUIElementTypeButton[@name=&quot;Track an Item&quot;]</value>
   </webElementProperties>
</WebElementEntity>
