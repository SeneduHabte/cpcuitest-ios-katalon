<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>imgBtn-NotNow</name>
   <tag></tag>
   <elementGuidId>72410f76-c843-441c-8642-db0543155472</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//XCUIElementTypeButton[@name=&quot;Not now&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//XCUIElementTypeButton[@name=&quot;Not now&quot;]</value>
   </webElementProperties>
</WebElementEntity>
