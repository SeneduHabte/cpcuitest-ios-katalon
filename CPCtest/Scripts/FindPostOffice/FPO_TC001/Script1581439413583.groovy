import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.ios.IOSDriver as IOSDriver
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper as MobileElementCommonHelper
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import org.openqa.selenium.remote.RemoteWebDriver

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', false)
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeButton - Open menu'), 0)

Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeStaticText - Find a post office'), 0)

Mobile.setText(findTestObject('FindaPostOffice/XCUIElementTypeSearchField - Enter address city or place'), 'M5G 1Z3', 
    3)
driver.context("NATIVE_APP")

driver.findElementByXPath("//*[@name='Search']").click()
//driver.context("NATIVE_APP_INSTRUMENTED")
//driver.findElement(By.xpath("//*[@name='Search']")).click()

//Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeButton - Search'), 0)

//driver.context(“WEBVIEW_1”)

Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeOther - CARLTON PO HASTY MARKET'), 19)

//Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeOther - EATON CENTRE PO SHOPPERS DRUG MART 0824'), 3)

//Mobile.tap(findTestObject('FindaPostOffice/XCUIElementTypeOther - CABBAGETOWN PO SHOPPERS DRUG MART 0853'), 3)

//Mobile.closeApplication()

