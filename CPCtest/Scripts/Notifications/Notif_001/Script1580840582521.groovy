import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.ios.IOSDriver as IOSDriver
//import io.appium.java_client.AppiumDriver 
//import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper as MobileElementCommonHelper
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.detroitlabs.katalonmobileutil.device.App as App
import com.detroitlabs.katalonmobileutil.device.Device as Device

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

Mobile.tap(findTestObject('Object Repository/Track/imgBtn-TrackItem'), 3)

Mobile.setText(findTestObject('Object Repository/Track/txt-TrackNumber'), '7023212201906061', 3)

driver.context("NATIVE_APP")
driver.findElementByXPath("//*[@name='Next:']").click()
Mobile.setText(findTestObject('Object Repository/Track/txt-Desc'), 'ProdTrack1', 3)

Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Track'), 3)

