import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import io.appium.java_client.AppiumDriver as AppiumDriver

/**Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', false)

if(Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL)==true)
{
	Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'),3)
Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'),6)
}

//Mobile.selectListItemByLabel(null, null, 30)
'Initializing Appium Driver by Katalon Mobile Driver'
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

if(Mobile.verifyElementExist(findTestObject('Object Repository/Track/XCUIElementTypeCell'), 20, FailureHandling.OPTIONAL)==true)
{
'Getting all similar elements and storing in to List'
List<WebElement> elements = driver.findElementsByXPath("//*[@type = 'XCUIElementTypeCell']")

'Printing the Size of list elements'
println('The size of elements is ::' + elements.size())
}
*/
Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL) == 
true) {
    Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 3)

    Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'), 6)
}

//Mobile.callTestCase(findTestCase('Track/Empty-the-List'), [('variable') : ''], FailureHandling.OPTIONAL)
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number'), 30)

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Tracking number'), 'CJ230853042US', 30)

driver.context('NATIVE_APP')

driver.findElementByXPath('//*[@name=\'Next:\']').click()

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Description (optional)0 of 30 characters'), 'Test 1- Add manually', 
    30)

//Mobile.setText(findTestObject(‘android.widget.EditText0 - hhhhhhhhhhhhh (1)’), ‘shamim’, 30)
Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track'), 30)

String text = Mobile.getAttribute(findTestObject('Object Repository/Track/XCUIElementTypeStaticText - Test 1- Add manually'), 
    'name', 30)

Mobile.verifyEqual(text, 'Test 1- Add manually', FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number'), 30)

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Tracking number'), 'CP854849168US', 30)

driver.context('NATIVE_APP')

driver.findElementByXPath('//*[@name=\'Next:\']').click()

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Description (optional)0 of 30 characters'), 'Test 2 - Add manually', 
    30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track'), 30)

text = Mobile.getAttribute(findTestObject('Object Repository/Track/XCUIElementTypeStaticText - Test 2 - Add manually'), 
    'name', 30)

Mobile.verifyEqual(text, 'Test 2- Add manually', FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number'), 30)

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Tracking number'), '7023210942177100', 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track'), 30)

//Mobile.getText(findTestObject('Track/XCUIElementTypeStaticText - 7023210942177100'), 30)
text = Mobile.getAttribute(findTestObject('Track/XCUIElementTypeStaticText - 7023210942177100'), 'name', 30)

Mobile.verifyEqual(text, '7023210942177100', FailureHandling.OPTIONAL)

