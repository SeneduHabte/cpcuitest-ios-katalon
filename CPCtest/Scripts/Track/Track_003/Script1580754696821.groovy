import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

Mobile.tap(findTestObject('Track/txtBtn-TurnonNotif'), 5)

Mobile.tap(findTestObject('Track/txtBtn-Allow'), 5)

boolean trackItem = Mobile.verifyElementExist(findTestObject('null'), 10)

if (trackItem == true) {
    Mobile.tap(findTestObject('null'), 0)
}

Mobile.setText(findTestObject('Track/txt-TrackNumber'), GlobalVariable.ProdPin, 0)

//not_run: Mobile.tap(findTestObject('null'), 0)
//Mobile.focus(findTestObject('Track/txt-Desc'))

Mobile.setText(findTestObject('Track/txt-Desc'), 'PRODPIN', 0)

Mobile.tap(findTestObject('null'), 0)

Mobile.getText(findTestObject('null'), 0)

Mobile.getText(findTestObject('null'), 0)

Mobile.closeApplication()

not_run: Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

not_run: Mobile.tap(findTestObject('null'), 0)

not_run: Mobile.setText(findTestObject('null'), '105351756232372', 0)


not_run: Mobile.setText(findTestObject('null'), 'ProdPin', 0)

not_run: Mobile.getText(findTestObject('null'), 0)

not_run: Mobile.getText(findTestObject('null'), 0)

not_run: Mobile.closeApplication()

