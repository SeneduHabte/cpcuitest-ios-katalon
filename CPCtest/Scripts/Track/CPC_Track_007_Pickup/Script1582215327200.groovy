import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/**Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number (1)'), 30)

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Tracking number (1)'), '7023210942177108', 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track (1)'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeStaticText - 7023210942177108'), 30)

Mobile.closeApplication()*/

Mobile.startApplication('//Users/seneduhabtegiorgis/Library/Developer/Xcode/DerivedData/CPCTools-ajwliedyysnnjjhhlyohjfqyhjyc/Build/Products/Debug-iphonesimulator/CPCTools.app', 
    true)

if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL) ==
	true) {
		Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 3)
	
		Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'), 6)
	}
	
Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number (1)'), 30)
//Mobile.tap(findTestObject('Object Repository/Track/txt-TrackNumber'), 30)

Mobile.setText(findTestObject('Object Repository/Track/txt-TrackNumber'), '7023210942177108', 30)

//Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Tracking number (1)'), '7023210942177108', 30)

//Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track (1)'), 30)
Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Track'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeStaticText - 7023210942177108'), 30)

Mobile.tap(findTestObject('Track/imgBtn-addToAppleWallet'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Cancel'), 30)

Mobile.tap(findTestObject('Track/imgBtn-addToAppleWallet'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Add'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track (3)'), 30)

Mobile.closeApplication()

