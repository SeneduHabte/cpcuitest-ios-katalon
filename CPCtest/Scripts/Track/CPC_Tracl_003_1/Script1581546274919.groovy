import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import org.openqa.selenium.WebElement as WebElement
import io.appium.java_client.AppiumDriver as AppiumDriver
//import java.util
import io.appium.java_client.MobileSelector as MobileSelector

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', false)

if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL) == 
true) {
    Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 3)

    Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'), 6)
}

//Mobile.selectListItemByLabel(null, null, 30)
'Initializing Appium Driver by Katalon Mobile Driver'
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/XCUIElementTypeCell'), 20, FailureHandling.OPTIONAL) == 
true) {
    //List<MobileElement> elementsone = (List<MobileElement>) driver
    //List elements = driver.findElementsByXPath("//*[@type = 'XCUIElementTypeCell']")
    'Getting all similar elements and storing in to List'
    List<WebElement> elements = driver.findElementsByXPath('//*[@type = \'XCUIElementTypeCell\']')

    //XCUIElementTypeStaticText
    'Printing the Size of list elements'
    println('The size of elements is ::' + elements.size())

    /**3.Identify by index
Sometimes, you will have multiple elements with same locator value. For example there may be two submit buttons with id starting with ‘Submit’. In this case you can use findElements method and locate the element using the index.
driver.findElements(By.xpath(//*[contains(@id, ‘submit’).get(0).click();

Here get(0) is used to get the first web element with matching XPath.
*/
    for (int j = 0; j < elements.size(); j++) {
        String m = Mobile.getText(findTestObject('//*[@xpath = \'XCUIElementTypeStaticText(2)\''), 30, FailureHandling.OPTIONAL)

        //String m = driver.findElementByXPath("//*[contains(@xpath,'XCUIElementTypeStaticText'),]",j).getText();
        println(m)
    } //XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeStaticText[2]
    /**for (WebElement anElement : elements) {
	
	println(anElement.getProperty("name");
	//if (anElement.getAttribute("name").equals("text")) {

			// Do something
}*/
}

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

Mobile.tap(findTestObject('Track/XCUIElementTypeStaticText'), 0)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Delete (1)'), 0)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Delete (2)'), 0)

Mobile.closeApplication()

