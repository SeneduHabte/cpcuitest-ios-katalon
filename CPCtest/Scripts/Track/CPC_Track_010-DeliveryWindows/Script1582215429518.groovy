import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.testdata.ExcelData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil


CustomKeywords.'cpc.mobile.startapp.startApptrue'()

//AppiumDriver<?> driver = MobileDriverFactory.getDriver()


ExcelData excelData = ExcelFactory.getExcelDataWithDefaultSheet('Data Files/Track/TrackPIns.xlsx', 'TrackStg',

	true)

 

Unavailable = excelData.getValue('TrackPIN', 8)
OnlyDate = excelData.getValue('TrackPIN', 9)
ByEndOfDay = excelData.getValue('TrackPIN', 10)
DateAndTime = excelData.getValue('TrackPIN', 11)

//println(SignaturePIN)

Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number'), 30)

Mobile.setText(findTestObject('Object Repository/Track/txt-TrackNumber'), Unavailable, 30)


Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Track'), 30)

String xpath = '//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[2]'
println(xpath);
TestObject to = new TestObject("objectName")

to.addProperty("xpath", ConditionType.EQUALS, xpath)
Mobile.tap(to, 30)





