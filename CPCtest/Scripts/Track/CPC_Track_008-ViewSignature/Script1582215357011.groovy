import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
	import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testdata.ExcelData
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil

import io.appium.java_client.AppiumDriver

Mobile.startApplication('//Users/matthews/Library/Developer/Xcode/DerivedData/CPCTools-dtpxfviqodylejanqwbyilwpxzsj/Build/Products/Debug-iphonesimulator/CPCTools.app', 
    true)
if (Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL) ==
	true) {
		Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 3)
	
		Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'), 30)
	}
	
	
	AppiumDriver<?> driver = MobileDriverFactory.getDriver()
	
	
	ExcelData excelData = ExcelFactory.getExcelDataWithDefaultSheet('Data Files/Track/TrackPIns.xlsx', 'TrackStg',
	
		true)
	
	 
	
	SignaturePIN = excelData.getValue('TrackPIN', 5)
	
	println(SignaturePIN)
	
	Mobile.tap(findTestObject('Track/XCUIElementTypeSearchField - Enter tracking number'), 30)

	Mobile.setText(findTestObject('Object Repository/Track/txt-TrackNumber'), SignaturePIN, 30)
	
	
	Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Track'), 30)
	
	String xpath = '//XCUIElementTypeCell[1]/XCUIElementTypeStaticText[2]'
	println(xpath);
	TestObject to = new TestObject("objectName")
	
	to.addProperty("xpath", ConditionType.EQUALS, xpath)
	
	Mobile.tap(to, 30)//, FailureHandling.OPTIONAL)
//Mobile.tap(findTestObject('Track/XCUIElementTypeStaticText - 7023210938954102'), 30)
//Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track'), 30)


Mobile.tap(findTestObject('Track/tabBtn-Detail'), 30)

Mobile.tap(findTestObject('Track/txtBtn-viewSignature'), 30)

Mobile.setText(findTestObject('Track/txtField-DestPosCode'), 'A1B2C3', 30)

Mobile.tap(findTestObject('Track/txtBtn-Continue'), 30)

String errDesPosCod = Mobile.getAttribute(findTestObject('Track/errMsg-EnterDestPosCode'), 'name', 30)

println(errDesPosCod)

String verEq=Mobile.verifyEqual(errDesPosCod,'  Enter the destination postal code.' , FailureHandling.OPTIONAL)

if (verEq==true)
{
	KeywordUtil.markPassed("Displayed error message correct")
	//Mobile.comment('Displayed error message correct')
}

if (verEq!=true)
{
	KeywordUtil.markPassed("Displayed error message correct")
	//Mobile.comment('Displayed error message Not correct')
}


Mobile.tap(findTestObject('Track/txtBtn-xClear'), 30)
Mobile.setText(findTestObject('Track/txtField-DestPosCode'), 'G6X3A1', 30)

Mobile.tap(findTestObject('Track/txtBtn-Continue'), 30)
Mobile.tap(findTestObject('Track/tabBtn-Detail'), 30)

Mobile.takeScreenshot("/Users/matthews/Documents/Screenshots/signature.png")

//Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Return to details'), 30)

//Mobile.tap(findTestObject('Track/txtBtn-viewSignature'), 30)

String delDate = Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-DelivDate'), 'name', 30)
println(delDate)

//Display Delivery

String expected, actual

expected = 'June 07'
actual = Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-DelivDate'), 'name', 30)

//CustomKeywords.'cpc.mobile.startapp.verifyDisplayElemment'(expected, actual)

CustomKeywords.'cpc.mobile.startapp.verifyDisplayElemment'(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-DelivDate'), 'June 07'), 30)
	
/**if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-DelivDate'), 'name', 30)=='June 07') {
	
	KeywordUtil.markPassed("Correct Delivery Date")
	}
else
{
	KeywordUtil.markFailed("INCorrect Delivery Date")
}

if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-DelivStat'), 'name', 30)=='Item is with your concierge or building manager') {
	
	KeywordUtil.markPassed("Correct Delivery Status")
	}
else
{
	KeywordUtil.markFailed("INCorrect Delivery Status")
}

if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-Sender'), 'name', 30)=='IIQA CUST DO NOT USE - 005') {
	
	KeywordUtil.markPassed("Correct Sender")
	}
else
{
	KeywordUtil.markFailed("INCorrect Sender")
}


if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-SignName'), 'name', 30)=='GHJ CGH') {
	
	KeywordUtil.markPassed("Correct Signatory Name")
	}
else
{
	KeywordUtil.markFailed("INCorrect Signatory Name")
}

//Mobile.scrollToText('Features and options', FailureHandling.OPTIONAL)

if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-shipServ'), 'name', 30)=='Expedited Parcels') {
	
	KeywordUtil.markPassed("Correct Ship Serv")
	}
else
{
	KeywordUtil.markFailed("INCorrect Ship Serv")
}
if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-SignReq'), 'name', 30)=='Signature Required') {
	
	KeywordUtil.markPassed("Correct Signature Required")
	}
else
{
	KeywordUtil.markFailed("INCorrect Signature Required")
}
if(Mobile.getAttribute(findTestObject('Object Repository/Track/txtDis-RefNum'), 'name', 30)=='ELIGIBLE PC') {
	
	KeywordUtil.markPassed("Correct Reference Number")
	}
else
{
	KeywordUtil.markFailed("INCorrect Reference Number")
}*/
Mobile.tap(findTestObject('Object Repository/Track/imgBtn-leftArrow'), 30)
//Mobile.pressBack()

/**Mobile.tap(findTestObject('Track/txtBtn-xClear'), 30)

Mobile.setText(findTestObject('Track/XCUIElementTypeTextField - Destination postal codeExample A1B2C3ErrorEnter the destination postal code.'), 
    'G6X3A1', 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Continue (1)'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Return to details'), 30)

Mobile.tap(findTestObject('Track/XCUIElementTypeButton - Track (2)'), 30)

Mobile.closeApplication()*/

