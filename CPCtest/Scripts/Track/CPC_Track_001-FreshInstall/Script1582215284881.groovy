import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling

Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)

if(Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL)==true)
{
	Mobile.tap(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'),30)
Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Allow'),30)
Mobile.takeScreenshot('/Users/matthews/Documents/Screenshots/TurnOn.png')

Mobile.closeApplication()
Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)
}

if(Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL)==true)
{
	Mobile.tap(findTestObject('Object Repository/Track/txtBtn-Not Now'),30)
	
	Mobile.takeScreenshot('/Users/matthews/Documents/Screenshots/TurnOff.png')
	
Mobile.closeApplication()
Mobile.startApplication('/Users/matthews/Desktop/CPCTools.app', true)
}


if(Mobile.verifyElementExist(findTestObject('Object Repository/Track/txtBtn-TurnonNotif'), 20, FailureHandling.OPTIONAL)==true)
{
	Mobile.tap(findTestObject('Object Repository/Track/imgBtn-CloseX'),30)
	
	Mobile.takeScreenshot('/Users/matthews/Documents/Screenshots/Close.png')

Mobile.closeApplication()
}
